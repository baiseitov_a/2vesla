import slick from 'slick-carousel';

import arrowLeft from '../../img/arrow-left.png';
import arrowRight from '../../img/arrow-right.png';

$(function () {

  // PROMO SLIDER
  $('.js-promo-slider').slick({
    infinite: true,
    arrows: false,
    dots: true
  });

  // PRODUCTS SLIDER
  $('.js-products-slider').slick({
    infinite: true,
    arrows: false,
    slidesToShow: 4,
    slidesToScroll: 1,
    dots: true,
    equalizeHeight: true,
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 960,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 650,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });

  /* PRODUCT DETAIL PAGE SLIDER */

  const thumbsSlider = $('.js-product-detail-main-slider').find('.product-detail__slider-item').clone();
  $('.js-product-detail__slider-wrap').append(`<div class="js-product-detail-thumb-slider"></div>`);
  $('.js-product-detail-thumb-slider').append(thumbsSlider);

  $('.js-product-detail-main-slider').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    asNavFor: '.js-product-detail-thumb-slider'
  });

  $('.js-product-detail-thumb-slider').slick({
    slidesToShow: 5,
    slidesToScroll: 1,
    asNavFor: '.js-product-detail-main-slider',
    dots: false,
    prevArrow: `<div class="slick-prev"><img src=${arrowLeft}/></div>`,
    nextArrow: `<div class="slick-next"><img src=${arrowRight}/></div>`,
    focusOnSelect: true
  });
})