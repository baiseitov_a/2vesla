import $ from 'jquery';
import './map';
import './range-filter';
import './sliders';
import select2 from 'select2';

$(document).ready(function () {

  $('.js-select').select2({
    minimumResultsForSearch: -1
  });

  /****** ***Ввод в инпут только цифр ***********/
  $('.js-number').keypress(function (e) {
    var charCode = (e.which) ? e.which : e.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
  })

  $('.js-counter .counter__plus').click(function () {
    const input = $(this).siblings('.counter__input');
    const value = +input.val();
    input.val(value + 1);
  })

  $('.js-counter .counter__minus').click(function () {
    const input = $(this).siblings('.counter__input');
    const value = +input.val();
    input.val(value <= 1 ? 1 : value - 1);
  })

  $('.js-accordion .accordion__top').click(function () {
    $(this).parents('.js-accordion').toggleClass('show');
    $(this).siblings('.accordion__content').slideToggle();
  });

  $('.js-header-mob__burger').click(function () {
    $('body').toggleClass('show-mobile-catalog')
  })

  $('.js-catalog__filter-toggle').click(function(){
    $('body').addClass('show-filter');
  })

  $('.js-filter-close').click(function(){
    $('body').removeClass('show-filter');
  })

  // tabs
  $('.js-tabs .tab-link').click(function (e) {
    e.preventDefault();
    var id = $(this).attr('href');
    var wrap = $(this).parents('.js-tabs');
    wrap.find('.tab-link').removeClass('active');
    $(this).addClass('active');
    wrap.find('.tab-block').removeClass('active');
    $(id).addClass('active');
  });

});