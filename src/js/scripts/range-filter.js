import ionRangeSlider from 'ion-rangeslider';

$(function () {

  var $filterTag = $('.filter-tag');
  var $rangeSlider = $('.filter-range__slider');
  var $rangeInputFrom = $('.filter-range__value.from input');
  var $rangeInputTo = $('.filter-range__value.to input');

  var $filterClearBtn = $('.filter-clear');

  $rangeInputFrom.val($rangeSlider.data('min'));
  $rangeInputTo.val($rangeSlider.data('max'));

  $rangeSlider.ionRangeSlider({
    type: "double",
    onChange: function (data) {
      $rangeInputFrom.val(data.from);
      $rangeInputTo.val(data.to);
    },
    onFinish: function (data) {
      //
    }
  });

  // Записываем инстанс в переменную
  var range = $rangeSlider.data("ionRangeSlider");

  $rangeInputFrom.on('input propertychange', function () {
    if ($(this).val() < $rangeSlider.data('min')) {
      range.update({
        from: $rangeSlider.data('min')
      })
    }
    else {
      range.update({
        from: $(this).val()
      })
    }
  })
  $rangeInputTo.on('input propertychange', function () {
    if ($(this).val() > $rangeSlider.data('max')) {
      range.update({
        to: $rangeSlider.data('max')
      })
    }
    else {
      range.update({
        to: $(this).val()
      })
    }
  })

  $rangeInputFrom.change(function () {
    if ($(this).val() < $rangeSlider.data('min')) {
      $(this).val($rangeSlider.data('min'))
    }
  })
  $rangeInputTo.change(function () {
    if ($(this).val() > $rangeSlider.data('max')) {
      $(this).val($rangeSlider.data('max'))
    }
  })

  // Очистка фильтра
  $filterClearBtn.click(function () {
    $filterTag.removeClass('active');
    range.update({
      from: $rangeSlider.data('min'),
      to: $rangeSlider.data('max')
    })
    $rangeInputFrom.val($rangeSlider.data('min'));
    $rangeInputTo.val($rangeSlider.data('max'));
    return false
  })


})