import mapMapker from '../../img/map-marker.png';

$(function () {  
  if ($('#map').length) {
    const map = $('#map');
    const mLat = parseFloat(map.attr('data-geo-lat'));
    const mLng = parseFloat(map.attr('data-geo-lng'));
    ymaps.ready(function () {
      const myMap = new ymaps.Map('map', {
        center: [mLat, mLng],
        zoom: 15,
        controls: [],
      });
      const myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
        hintContent: '',
        balloonContent: ''
      }, {
          iconLayout: 'default#image',
          iconImageHref: mapMapker,
          iconImageSize: [67, 67],
        })
      myMap.geoObjects.add(myPlacemark);
    });
  }
})